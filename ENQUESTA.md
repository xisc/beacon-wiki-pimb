


Enquesta template: 

```yaml
institut: <nom>
maquina:
  tipus: < desktop | servidor-dedicat | servidor-compartit | cluster >
  SO:
    nom:    { debian   | sles | redhat | bsd  | etc... }
    versió: { bullseye | 15.2 | 9.0    | 13.0 | etc... }
  network:
    adreça ip (externa): 
      - si està darrer d'un tallafoc, serà l'adreça configurada al tallafoc.
      - opcional (per ara ;))
  software:
    - docker:           
        instal·lat?: <si|no>
        versió: <string-versió> 
    - docker-compose (amb guió): 
        instal·lat?: <si|no>
        versió: <string-versió> 
    - docker compose (amb espai):  
        instal·lat?: <si|no>
        versió: <string-versió>  
    - beacon2-ri-tools: 
        baixat?: <si|no>
    - beacon2-ri-api:   
        baixat?: <si|no>
  dades:
    - CINECA:
        baixat?: <si|no>
  ajuda:
    - 1 - res de res, 5 - Ajuda'm Obi Wan...
    necessitem / volem ajuda: <1-5>
  entorn:
    - <periode> := 1h, 6d, 2s, 3m, etc...
    tenim previst un entorn funcional / test dintre de: <periode>
```

e.g.,

```yaml
institut: SJD
maquina:
  tipus: servidor-compartit 
  SO:
    nom: sles 
    versió: 15
  network:
    adreça ip (externa): 364.12.1.92 
  software:
    - docker:           
        instal·lat?:  si
        versió:       19.03.11
    - docker-compose: 
        instal·lat?:  si
        versió:       1.25.3
    - docker compose:  
        instal·lat?: si
        versió:      v2.6.0
    - beacon2-ri-tools: 
        baixat: si
    - beacon2-ri-api:   
        baixat: si
  dades:
    - CINECA:
        baixat?: si
  ajuda:
    necessitem / volem ajuda (1-5): 4
  entorn:
    tenim previst un entorn funcional / test dentre de: 2m
```

Comandes / ordres útils:

linux-like:
  - `cat /etc/os-release | grep -e '^ID=' -e '^VERSION_ID='`
  - `docker --version`
  - `docker-compose --version` # només necessitem un
  - `docker compose --version` # només necessitem un (<- preferit)
  - `ip addr`
  - `hostname -i`
  - `git clone https://github.com/EGA-archive/beacon2-ri-tools`
