### What is a Beacon?

The [official documentation](http://docs.genomebeacons.org/what-is-beacon-v2/) states:

> Beacon v2 is a protocol/specification established by the Global Alliance for Genomics and Health initiative (GA4GH) that defines an open standard for federated discovery of genomic (and phenoclinic) data in biomedical research and clinical applications.

These specifications:

> - are a set of human & machine-readable documents
> - define a standardised format with which to structure genomics data
> - define a structured way to reference the data using a REST-API

